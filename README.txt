CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Design Decisions


INTRODUCTION
------------

Current Maintainers: Raphael D�erst <raphael.duerst@gmail.com> Jonas B�rtsch <jonas.baertsch@innoveto.com>

The webform status module adds a simple block to the site to display the submission satus of selected webforms to the user.


INSTALLATION
------------
Enable the module on the module page
Configure the module

DESIGN DECISIONS
----------------

