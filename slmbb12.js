(function($) {

  hideStatusBlock = function (data) {
    $.ajax({
      type: 'POST',
      url: Drupal.settings.slmbbhideurl,
      dataType: 'json',
      success: processResponse
    });
  }

  processResponse = function (data) {
    $('#block-slmbb12-slmbb').html('<div>'+data.message+'</div>');
  }

})(jQuery);
